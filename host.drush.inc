<?php

/**
 * @file
 *   custom commands for Drush.
 */


/**
* Implements hook_drush_command().
*
* @See drush_parse_command() for a list of recognized keys.
*
* @return
*   An associative array describing each command.
*/
function host_drush_command() {
  $items = array();

  $items['host-add'] = array(
    'description' => "Add to your device's host file through Drush.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array(
      'ha',
    ),
    'scope' => 'system',
    'arguments' => array(
      'url' => 'The url to set in the host file.',
    ),
    'required-arguments' => TRUE,
    'options' => array(
      'host_file' => "The location of the hosts file. Should be editable without using sudo.",
      'address' => "IP Address of the destination. Defaults to '127.0.0.1' if empty.",
      'reset' => "The command to flush the DNS cache. If you need sudo for this, then leave this be, and you'll need to do that yourself.",
      'hf' => array(
        'description' => "An abbreviation of the hostFile option.",
        'hidden' => TRUE,
      ),
      'a' => array(
        'description' => 'A shorthand for the address option.',
        'hidden' => TRUE,
      ),
      'res' => array(
        'description' => "An abbreviation of the reset option.",
        'hidden' => TRUE,
      ),
    ),
    'examples' => array(
      'drush host-add example.com' => 'Tries to find the host file based on the system, and adds the example.com site to point to localhost server.',
      'drush ha example.com --host_file="/etc/hosts" --address="127.0.0.1" --reset="dscachutil -flushcache"' => 'Uses the default values for the full option names, dns is using the OSX 10.6 command.',
      'drush ha --hf="/etc/hosts" --a="127.0.0.1" --res="dscachutil -flushcache"' => 'Same as above, just using abbreviated forms of options.'
    ),
  );

  $items['host-remove'] = array(
    'description' => "Remove a site to the vhost configuration file using Drush.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'aliases' => array(
      'hr',
    ),
    'arguments' => array(
      'url' => 'The access url of the site to remove.',
    ),
    'required-arguments' => TRUE,
    'options' => array(
      'host_file' => "The location of the hosts file. Should be editable without using sudo.",
      'reset' => "The command to flush the DNS cache. If you need sudo for this, then leave this be, and you'll need to do that yourself.",
      'hf' => array(
        'description' => "An abbreviation of the host_file option.",
        'hidden' => TRUE,
      ),
      'res' => array(
        'description' => "An abbreviation of the reset option.",
        'hidden' => TRUE,
      ),
    ),
    'examples' => array(
      'drush custom-host-remove example.com' => 'Attempts to locate the hosts file and alter it so the example.com entry is not there.',
      'drush chr example.com --host_file="/etc/hosts" --reset="dscachutil -flushcache"'
                                    => 'Removes example.com from the given hosts file, and flushes the DNS cache with the given OSX 10.6 command.',
      'drush chr example.com --hf="/etc/hosts" --res="dscachutil -flushcache"'
                                    => 'Same as above, but using abbreviated options.',
    ),
    'scope' => 'system',
  );

  $items['host-vhost-add'] = array(
    'description' => 'Add a vhost entry to the apache configurations.',
	'bootstrap'   => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'ServerName' => 'What will this server be named; doubles as the access url of the main site.',
	),
	'required-arguments' => TRUE,
	'options' => array(
	  'DocumentRoot' => 'The document root of this virtual host.',
      'host' => 'Add a host entry, as per the host-add command.',
	  'aliases' => 'Additional sites to create, typically related but independent.',
	  'sites-dir' => "The directory site's information is stored in.",
	  'vhost' => 'Where the vhost configuration file is, used when sites-dir is 0 or FALSE',
	  'ErrorLog' => 'An error log.',
	  'AccessLog' => 'An access log.',
	  'drupalVersion' => 'An integer for what version of Drupal will host this site.',
	  'dr' => array(
        'description' => 'An abbreviation of the DocumentRoot option.',
		'hidden' => TRUE,
	  ),
	  'h' => array(
	    'description' => 'An abbreviation of the host option.',
		'hidden' => TRUE,
	  ),
	  'sd' => array(
	    'description' => 'An abbreviation of the sites-dir option.',
	    'hidden' => TRUE,
	  ),
	  'vh' => array(
        'description' => 'An abbreviation of the vhost option.',
		'hidden' => TRUE,
	  ),
	  'el' => array(
        'description' => 'An abbreviation of the ErrorLog option.',
		'hidden' => TRUE,
	  ),
	  'al' => array(
        'description' => 'An abbreviation of the AccessLog option.',
		'hidden' => TRUE,
	  ),
	  'dv' => array(
	    'description' => 'An abbreviation of the DrupalVersion option.',
	    'hidden' => TRUE,
	  ),
	),
	'allow-additional-options' => array(
	  'host-add',
	),
	'examples' => array(
	  'drush host-vhost-add example.com --DocumentRoot=/var/www/example.com --sites-dir=/etc/apache2/sites-available' =>
	    dt('A way to quickly add a new Drupal site to the local computer for development; using minimum options.'),
	  'drush hva example.com --DocumentRoot=/var/www/example.com --aliases=prod,dev,demo --sites-dir=/etc/apache2/sites-available ErrorLog=/var/www/example.com/logs/error.log AccessLog=/var/www/example.com/logs/access.log --drupalVersion=7' =>
	    dt('All options in use, no abbreviations.'),
	  'drush hva example.com --dr=/var/www/example.com --aliases=prod,dev,demo --sd=/etc/apache2/sites-available el=/var/www/example.com/logs/error.log al=/var/www/example.com/logs/access.log --dv=7' =>
	    dt('Same as above, but using abbreviations.'),
	  'drush hva example.com --DocumentRoot=/var/www/example.com --sites-dir=/etc/apache2/sites-available --host' =>
	    dt('Also make additions to the host file so this computer will resolve to the site properly.'),
	),
	'aliases' => array(
	  'hva',
	),
	'scope'       => 'system',
  );
  
  /**
   * This is specific to my system for now, I may revisit this to generalize it later.
   */
  $items['host-vhost-remove'] = array(
    'description' => 'Quick way to undo changes done by the host-vhost-add command.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'ServerName' => 'The ServerName for the site to remove.',
    ),
    'required-arguments' => TRUE,
    'options' => array(
      'vhost' => 'Modify the VHost file. Defaults to TRUE.',
      'host' => 'Modify the host file. Defaults to TRUE.',
      'filesystem' => 'Remove the files on the filesystem. Defaults to FALSE.',
      'vh' => array(
        'description' => 'Abbreviated form of the vhost option.',
        'hidden' => TRUE,
      ),
      'h' => array(
        'description' => 'Abbreviated form of the host option.',
        'hidden' => TRUE,
      ),
      'fs' => array(
        'description' => 'Abbreviated form of the filesystem option.',
        'hidden' => TRUE,
      ),
    ),
    'aliases' => array(
      'hvr',
    ),
  );

  // I always have a command of this sort to speed up the help system.
  $items['host'] = array(
    'description' => 'Custom commands to ease development workflow.',
    'bootstrap'   => DRUSH_BOOTSTRAP_DRUSH,
    'scope'       => 'system',
    'hidden'      => TRUE,
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * @param
 *   A string for user output
 *
 * @return
 *   A string with the help text for your command.
 */
function host_drush_help($section) {
  switch ($section) {
    // Basic help text
    case 'drush:host-add':
      return dt("Add an entry to a host file on your device, " .
	    "in order to override DNS settings.");
    case 'drush:host-remove':
      return dt("Removes an entry from the host file on your device, ".
	    "in order to restore DNS settings.");
	case 'drush:host-vhost-add':
	  return dt("Adds a vhost entry to apache configurations.");
    // Meta data
    case 'meta:host:title':
      return dt('Host commands');
    case 'meta:host:summary':
      return dt('Shortcuts to ease modifying the local machine for additional sites.');
    // Error codes
    case 'error:HOST_FILE':
      return dt('There was no file found where you said the hostFile was.');
  }
}


/**
 * Implements drush_COMMANDNAME().
 *
 * All of my drush commandfiles have a command like this.
 */
function drush_host() {
  $alias = array('@none');
  $command = 'help';
  $args = array();
  $options[] = '--filter=host';
  $results = drush_invoke_process($alias,$command,$args,$options);
  $results = $results['output'];
  drush_print('The following commands are in the host package. Use the `drush help <command>` for further information.');
  drush_print($results);
}

/**
 * Implements drush_COMMAND_validate().
 */
function drush_host_add_validate() {
  if ($error = drush_host_hostfile()) {
    return $error;
  }
}

/**
 * Implements drush_COMMAND().
 */
function drush_host_add() {
  // Load settings
  $path = drush_get_option(array('hostFile', 'hf'), '/etc/hosts');
  $address = drush_get_option(array('address', 'a'), '127.0.0.1');
  $args = drush_get_arguments();
  $url = $args['1'];
  // Make the line for addition.
  $addition = "\n$address\t$url";
  // Add it to the file
  if (drush_file_append_data($path, $addition)) {
    drush_log('Addition successful', 'success');
    if ($cmd = drush_get_option(array('reset', 'res'),FALSE)) {
      if (drush_shell_exec($cmd)) {
        drush_log("DNS Cache flushed.", 'success');
      } else {
        drush_log("DNS Cache couldn't be flushed.", 'warning');
      }
    } else { // No 'reset' set.
      $msg = 'Now you should flush your DNS Cache.';
      drush_print($msg);
    }
  } else { // Couldn't add entry to the file.
    drush_log('Addition failed.', 'warning');
    $msg = "You'll have to do this manually... add the following line to the\n"
      ."end of the file to proceed:$addition\n\n"
      ."Then remember to flush your DNS Cache.";
    drush_print($msg);
  }
}


/**
 * Implements drush_COMMAND_validate().
 */
function drush_host_remove_validate() {
  if ($error = drush_host_hostfile()) {
    return $error;
  }
}

/**
 * Implements drush_COMMAND().
 */
function drush_host_remove() {
  $args = drush_get_arguments();
  $url = $args['1'];
  $path = drush_get_option(array('host_file', 'hf'), '/etc/hosts');
  $file = file($path, FILE_IGNORE_NEW_LINES);
  $original = $file;
  foreach($file as $key => $value) {
    if (strpos($value, $url) === FALSE) {
      $result[] = $value;
    }
  }
  $test = array_diff($original, $result);
  if (!empty($test)) {
    // Write the result...
    drush_host_rewite($path, $result);
    if (drush_get_error() == DRUSH_SUCCESS && $reset = drush_get_option(array('reset', 'res'), FALSE)) {
      if (drush_shell_exec($reset)) {
        drush_log('DNS Cache flushed.', 'success');
      } else {
        drush_log("Couldn't flush the DNS cache.", 'warning');
      }
    }
  }
  else {
    drush_log('No change detected in the host file... do you have the correct url?');
  }
}

function drush_host_rewite($path, $result) {
  // Make the array into a string for use.
  if (is_array($result)) {
    $result = implode("\n",$result);
  }
  // Shell commands to execute, with intended args following.
  $cmd['cp'] = 'cp %s %s.bak';           // $path $path
  $cmd['output'] = 'echo -n %s > %s.new'; // $result $path
  $cmd['remove'] = 'rm -f %s';           // $path
  $cmd['replace'] = 'mv %s.new %s';      // $path $path
  $cmd['finish'] = 'rm -f %s.bak';       // $path
  // Combine the commands into one string.
  foreach($cmd as $next) {
    $command .= "&& $next";
  }
  unset($cmd);
  // Remove the leading '&& '
  $command = substr($command,3);
  /*
   * Now to execute, fully explain the procedure upon failure to aid
   * help troubleshooting, and correcting.
  */
  if (drush_shell_exec($command,
      /* For $cmd['cp']:      */ $path, $path,
      /* For $cmd['output']:  */ $result, $path,
      /* For $cmd['remove']:  */ $path,
      /* For $cmd['replace']: */ $path, $path,
      /* For $cmd['finish']:  */ $path
  )) {
    drush_log('Hosts file recreated.', 'success');
  } else {
    drush_log("There was an error along the way.", 'error');
    drush_print("The commands are written in such a way to minimize damage\n"
    ."upon failure, though it still deserves some attention. Each command\n"
    ."only executes on the success of the prior ones. The sequence attempted\n"
    ."detailed below.\n\n"
    ."\tFirst the existing file was backed up at $path.bak\n"
    ."\tSecond the new file was created at $path.new\n"
    ."\tThird the original existing was removed from $path\n"
    ."\tFourth the moving of $path.new to $path\n"
    ."\tFifth the removal of $path.bak");
  }
}

/**
 * A common validation step for multiple commands.
 */
function drush_host_hostfile() {
  /**
   * Default path for host files change from one system to another...
   * Use the general linux location, which also works on OSX
   */
  $path = drush_get_option(array('host_file','hf'),'/etc/hosts');
  if (!drush_file_not_empty($path)) {
    return drush_set_error('HOST_FILE');
  }
  return;
}

/**
 * Implementation of drush_COMMAND_validate.
 */
function drush_host_vhost_add_validate() {
  $error = drush_host_vhost_location();
  if (isset($error)) {
    return $error;
  }
  $error = drush_host_vhost_DocumentRoot();
  if (isset($error)) {
    return $error;
  }
  $error = drush_host_vhost_logs();
  if (isset($error)) {
    return $error;
  }
  drush_host_vhost_drupalversion();
}

/**
 * Validate the existence of a configuration file; also put the path in the vhost option
 * for ease of use later.
 */
function drush_host_vhost_location() {
  $args = drush_get_arguments();
  $ServerName = $args[1];
  $path = drush_get_option(array('sd','sites-dir'), '/etc/apache2/sites-available');
  if ($path) {
    if (drush_is_absolute_path($path)) {
      if (!drush_file_not_empty($path . '/' . $ServerName)) {
        drush_set_option('vhost',$path . '/' . $ServerName);
        return;
      }
      return drush_set_error('Site already exists.');
    }
    return drush_set_error('sites-dir needs to be an absolute path');
  }
  $path = drush_get_option(array('vh','vhost'), FALSE);
  if ($path) {
    if (drush_is_absolute_path($path) && drush_file_not_empty($path)) {
      drush_set_option('vhost',$path);
      return;
    }
    return drush_set_error("vhost config file doesn't exist.");
  }
  return drush_set_error('Missing an option; one of sites-dir or vhost is required.');
}

/**
 * Confirm DocumentRoot is set, and put it in the DocumentRoot option for later use.
 */
function drush_host_vhost_DocumentRoot() {
  $args = drush_get_arguments();
  $ServerName = $args[1];
  $path = drush_get_option(array('DocumentRoot', 'dr'),'/Users/tnanek/Sites/' . $ServerName);
  if ($path) {
    if (drush_is_absolute_path($path)) {
      drush_set_option('DocumentRoot',$path);
      return;
    }
    return drush_set_error('DocumentRoot needs to be an absolute path.');
  }
  return drush_set_error('DocumentRoot is required.');
}

/**
 * Confirm the log settings are valid, and put them in the full names if set for later use.
 */
function drush_host_vhost_logs() {
  $args = drush_get_arguments();
  $ServerName = $args[1];
  $logs['ErrorLog'] = drush_get_option(array('el','ErrorLog'), '/Users/tnanek/Sites/' . $ServerName . '/logs/error.log');
  $logs['AccessLog'] = drush_get_option(array('al','AccessLog'), '/Users/tnanek/Sites/' . $ServerName . '/logs/access.log');
  foreach($logs as $type => $path) {
    if ($path) {
      if (drush_is_absolute_path($path)) {
        drush_set_option($type, $path);
        if ($type == 'AccessLog') {
          drush_set_option('al', $path . ' combined');
        }
      }
      else {
        return drush_set_error('Invalid ' . $type . ' entry; ensure it is an absolute path.');
      }
    }
  }
  return;
}

/**
 * Load the Drupal version; one place to change the default.
 */
function drush_host_vhost_drupalversion() {
  /**
   * @todo
   * Change the default to be the latest version recommended or installed.
   */
  $default = '7';
  $version = drush_get_option(array('dv', 'drupalVersion'), $default);
  if (is_numeric($version)) {
    $version = $version + 0;
    if ($version > 5 || $version < 3) {
      if (!is_int($version)) {
        drush_set_option('DrupalVersion', $default);
        drush_log("Invalid DrupalVersion detected, using $default instead.");
        return;
      }
    }
    // Confirm it is a valid release...
    drush_set_option('drupalVersion', "$version");  
  }
  drush_set_option('DrupalVersion', $default);
  drush_log("Invalid DrupalVersion detected, using $default instead.");
}

/**
 * Pre-execution; ensure all referenced files exist, and if not, create them.
 *
 * @note: This is specific to my own system at the moment, though as generalized as I
 *        could make it.
 */
function drush_host_pre_host_vhost_add() {
  $home = getenv('HOME');
  $drupalVersion = drush_get_option('drupalVersion');
  $drupalSites = "$home/Sites/drupal-$drupalVersion.x/sites";
  $serverName = drush_get_arguments();
  $serverName = $serverName[1];
  $docRoot = drush_get_option(array('dr','DocumentRoot'), "$home/Sites/$serverName");
  $serverRoot = $docRoot;
  $docRoot = $docRoot . '/htdocs';
  drush_set_option('DocumentRoot', $docRoot);
  $directories = array(
    'sites' => "$serverRoot/sites",
    'logs' => "$serverRoot/logs",
    'profiles' => "$serverRoot/profiles",
  );
  $links = array(
    "$serverRoot/htdocs" => "../drupal-$drupalVersion.x",
  );
  $aliases = drush_get_option_list('aliases', 'prod,dev,demo');
  // Based on the aliases loaded above, establish the arguments for copying and linking
  foreach($aliases as $site) {
    $copies[ $directories['sites'] . "/$site"] = "$drupalSites/default";
    if ($site == 'prod') {
      $links["$drupalSites/$serverName"] = $directories['sites'] . "/prod";
    } else {
      $links[ "$drupalSites/$site.$serverName"] = $directories['sites'] . "/$site";
    }
  }
  // Get the logs, and potentially put them in a touches array, if set.
  $logs = array(
    'ErrorLog' => drush_get_option('ErrorLog', FALSE),
    'AccessLog' => drush_get_option('AccessLog', FALSE),
  );
  foreach($logs as $path) {
    if ($path) {
      $touches[] = $path;
    }
  }
  // The commands to execute.
  $cmd['mkdir'] = 'mkdir -p %s';
  $cmd['cp'] = 'cp -r %s %s';
  $cmd['touch'] = 'touch %s';
  $cmd['ln'] = 'ln -s %s %s';
  $cmd['mv'] = 'mv %s %s';
  // Make the directories.
  foreach($directories as $path) {
    drush_shell_exec($cmd['mkdir'], $path);
  }
  // Copy the default site to the new locations.
  foreach($copies as $target => $origin) {
    drush_shell_exec($cmd['cp'], $origin, $target);
  }
  // Touch the logs.
  foreach($touches as $log) {
    drush_shell_exec($cmd['touch'], $log);
  }
  // Establish the links to allow Drupal to function.
  foreach($links as $location => $target) {
    drush_shell_exec($cmd['ln'], $target, $location);
  }
  // Set up the site for installation.
  foreach($copies as $sitedir => $origin) {
    // Move default.settings.php to settings.php
    drush_shell_exec($cmd['mv'], $sitedir . "/default.settings.php", $sitedir . "/settings.php");
    // Make the directories
    drush_shell_exec($cmd['mkdir'], $sitedir . "/libraries");
    drush_shell_exec($cmd['mkdir'], $sitedir . "/themes");
    drush_shell_exec($cmd['mkdir'], $sitedir . "/modules");
    drush_shell_exec($cmd['mkdir'], $sitedir . "/files");
  }
  
  $text = drush_host_format_plural($aliases, 'Site is', 'Sites are');
  
  drush_log("$text set up for installation.", 'success');
  
}

/**
 * Modify the apache configuration to enable the new site to be accessed.
 */
function drush_host_vhost_add() {
  $args = drush_get_arguments();
  $settings = array(
    'ServerName' => $args[1],
    'ServerAlias' => "*." . $args[1],
    'DocumentRoot' => drush_get_option('DocumentRoot',FALSE),
    'ErrorLog' => drush_get_option('ErrorLog', FALSE),
    'CustomLog' => drush_get_option('al', FALSE),
  );
  $output[] = "";
  $output[] = "<VirtualHost *:80>";
  foreach($settings as $name => $value) {
    if ($value) {
      $output[] = "\t$name $value";
    }
  }
  $output[] = "</VirtualHost>";
  $output[] = "";
  
  $vhost = implode("\n", $output);
  
  if (drush_file_append_data(drush_get_option('vhost'), $vhost)) {
    drush_log('VHost configuration saved successfully', 'success');
  }
  else {
    drush_log("Couldn't write to the VHost file, copy the below to it yourself to\n" .
      "complete the installation.",'warning');
    drush_print(vhost);
  }
}

/**
 * Add the entries to the hosts file.
 * @todo
 * Potentially enhance this to provide an alias for the site(s).
 */
function drush_host_post_host_vhost_add() {
  if (drush_get_option('host','0')) {
    $serverName = drush_get_arguments();
    $serverName = $serverName[1];
    $aliases = drush_get_option_list(array('a', 'aliases'), 'prod,dev,demo');
    $sites = array();
    foreach($aliases as $instance) {
      if ($instance == 'prod') {
        $sites[] = array($serverName);
      }
      else {
        $sites[] = array($instance . '.' . $serverName);
      }
    }
    $cmd = 'drush host-add';
    drush_print_r($sites);
    foreach($sites as $entry) {
      drush_invoke_process(array('@none'), $cmd, $entry);
    }
    drush_log('Host file modifications complete.', 'success');
    drush_print("Now you'll need to install the sites. Visit the urls listed below.");
    foreach($sites as $entry) {
      drush_print('http://' . $entry . '/install.php', 2);
    }
  }
}

/**
 * Remove the Server from the vhost apache config file. Specific to my personal laptop, 
 * a MacBook using a MAMP install for local development.
 */
function drush_host_vhost_remove_vhost() {
  $vhost = '/Applications/MAMP/conf/apache/vhosts.conf';
  $args = drush_get_arguments();
  $ServerName = $args[1];
  $file = file($vhost, FILE_IGNORE_NEW_LINES);
  $search = "\tServerName $ServerName";
  $key = array_search($search, $file);
  if ($key === FALSE) {
    drush_log('No entry found in the vhost file.', 'warning');
    return;
  }
  $post = $file;
  $pre = $file;
  for($cnt = 0; $cnt < $key; $cnt++) {
    unset($post[$cnt]);
  }
  for($cnt = $key+1; $cnt < count($file); $cnt++) {
    unset($pre[$cnt]);
  }
  $pre = array_reverse($pre, TRUE);
  $search = '<VirtualHost *:80>';
  $trim['start'] = array_search($search, $pre);
  $search = '</VirtualHost>';
  $end = array_search($search, $post);
  $trim['end'] = $end;
  for ($cnt = $trim['start']; $cnt < $trim['end']+1; $cnt++) {
    $store[] = $file[$cnt];
    unset($file[$cnt]);
  }
  $output = implode("\n", $file);
  $cmd['rm'] = 'rm -f %s';
  if (drush_shell_exec($cmd['rm'], $vhost)) {
    drush_file_append_data($vhost, $output);
  }
  drush_log('VHost configuration modified.', 'success');
  return;
}

/**
 * Remove the files saved on the system.
 */
function drush_host_vhost_remove_filesystem() {
  $args = drush_get_arguments();
  $ServerName = $args[1];
  $home = getenv('HOME');
  $path = $home . '/Sites/' . $ServerName;
  $cmd['rm'] = 'rm -rf %s';
  drush_shell_exec($cmd['rm'], $path);
  drush_log("Files deleted", 'success');
}

/**
 *
 */
function drush_host_vhost_remove_host() {
  $args = drush_get_arguments();
  $ServerName = $args[1];
  $potential = array(
    array($ServerName),
    array("dev.$ServerName"),
    array("demo.$ServerName"),
  );
  foreach($potential as $entry) {
    drush_invoke_process(array('@none'), 'host-remove', $entry);
  }
  drush_log("Host file has been modified.", 'success');
}

/**
 * Implementation of hook_command().
 * Handle the vhost here.
 */
function drush_host_vhost_remove() {
  if (drush_get_option(array('vhost', 'vh'),TRUE)) {
    drush_host_vhost_remove_vhost();
  }
  if (drush_get_option(array('host', 'h'), TRUE)) {
    drush_host_vhost_remove_host();
  }
  if (drush_get_option(array('filesystem', 'fs'), FALSE)) {
    drush_host_vhost_remove_filesystem();
  }
}

/**
 * A helper function for output.
 * If a single string is passed, that will be returned.
 * If two strings are passed, and the array has a count == 1, then the first will be
 *  returned, else the second.
 * If three strings are passed, and 0 count, the first string, 1 count the second string,
 *  else the third.
 *
 * @param array $array
 *    Items to count.
 * @param string $first
 *    The lowest denominator string to return.
 * @param string $second
 *    The next denominator string to return.
 * @param string $third
 *    The largest denominator string to return.
 */
function drush_host_format_plural($array, $first, $second = NULL, $third = NULL) {
  $count = count($array);
  if ($second == NULL) {
    return $first;
  }
  if ($third == NULL) {
    if ($count == 1) {
      return $first;
    }
    return $second;
  }
  if ($count == 0) {
    return $first;
  }
  if ($count == 1) {
    return $second;
  }
  return $third;
}